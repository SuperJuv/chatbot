const express = require('express');
const config = require('../config.js');

const router = express.Router();

router.get('/', function(req, res, next) {
  // GET/api/ route
  res.send({name: config.admin.name});
});

module.exports = router
