const express = require("express");
const app = express();
const server = require("http").createServer(app);
const io = require('socket.io')(server);
const bodyParser = require("body-parser");
const api = require("./routes/api");
const Fuse = require('fuse.js')
const port = process.env.PORT || 3000;

const bot = require("socket.io-client")("http://127.0.0.1:3000",{
    query: {
        name: 'boti',
        isBot: true
    }
})

server.listen(port, () =>
    console.log(`Server is listening on port ${port}`)
);

app.use(express.static(__dirname + "/src"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/api", api);

let messages = []
let questions = []
let answers = {}
let lastIsQ = -1

io.on("connection", socket => {
    console.log('New client connected')
    socket.emit("messages", messages)
    if(!socket.handshake.query.isBot) {
        bot.emit("message",{userName:'boti',message:"Welcome to "+socket.handshake.query.name,noHistory:true})
    }
    socket.on("message", msg => {
        const history = !msg.hasOwnProperty('noHistory')
        history && messages.push(msg)
        io.emit("message",msg)
    })
    socket.on("disconnect", () => {
        console.log("Client disconnected");
    });
})

bot.on("message", msg => {
    if(msg.userName === 'boti') return
    const theText = msg.message
    const isQuestion = theText.slice(-1) === '?'
    const socketId = msg.id
    if(isQuestion) {
        //check if similar q asked
        const questionObject = getSimilarQuestionObject((theText))
        let questionIndex = questionObject.item ? questions.findIndex((q) => q.q === questionObject.item.q) : -1
        //add to questions array if not exist
        if(questionIndex === -1) questionIndex = questions.push({q:theText}) - 1
        //save index of last asked question
        lastIsQ = questionIndex
        //return available answers
        const questionAnswers = answers.hasOwnProperty(lastIsQ) ? answers[lastIsQ] : []
        if(questionAnswers.length > 0) {
            if(questionObject.item && questionObject.score !== 0) {
                io.to(`${socketId}`).emit("message",{userName:'boti',message:"obviously you cant type...\n\ryou meant to write :"+questionObject.item.q+"\n\rwrite those down :\n\r"+questionAnswers.join('\n\r')})
            } else {
                io.to(`${socketId}`).emit("message",{userName:'boti',message:"this is an old question, please use your eyes and fingers to scroll up and read...\n\rdo me a favor and read this, might put some common sense in to you :\n\r"+questionAnswers.join('\n\r')})
            }
        } else {
            io.to(`${socketId}`).emit("message",{userName:'boti',message:"no one know..."})
        }
    } else {
        if(lastIsQ >= 0) {
            const questionAnswers = answers.hasOwnProperty(lastIsQ) ? answers[lastIsQ] : []
            if(questionAnswers.length > 0) {
                io.to(`${socketId}`).emit("message",{userName:'boti',message:"i already know an answer, you are not spacial"})
            } else {
                io.to(`${socketId}`).emit("message",{userName:'boti',message:"i have nothing, so ill take your dumb answer"})
            }
            questionAnswers.push(theText)
            answers[lastIsQ] = questionAnswers
        } else {
            io.to(`${socketId}`).emit("message",{userName:'boti',message:"no one ask you!"})
        }
        lastIsQ = -1
    }
})

function getSimilarQuestionObject(text) {
    const options = {
        shouldSort: true,
        tokenize: true,
        matchAllTokens: true,
        includeScore: true,
        threshold: 0.6,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 1,
        keys: [
            "q"
        ]
    }
    const fuse = new Fuse(questions,options)
    const question = fuse.search(text)

    return question[0] || {}
}