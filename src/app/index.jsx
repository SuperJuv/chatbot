import React from "react";
import { render } from "react-dom";
import socketIOClient from "socket.io-client";
import { SocketProvider } from 'socket.io-react';
import faker from 'faker'

import "./../scss/main.scss";

import ChatClient from "./Components/ChatClient"

const userName = faker.name.findName()

const socket = socketIOClient('http://127.0.0.1:3000',{
    query: {
        name: userName
    }
});

class App extends React.Component {
  render() {
    return (
      <div className="home">
          <SocketProvider socket={socket}>
              <ChatClient />
          </SocketProvider>
      </div>
    );
  }
}

render(<App />, document.getElementById("app"));
