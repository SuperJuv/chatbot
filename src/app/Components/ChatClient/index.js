import React, {Component} from 'react';
import { socketConnect } from 'socket.io-react';
import ChatWindow from "../ChatWindow"

import './style.scss'

class ChatClient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputText: ''
        }

        this.sendMessage = this.sendMessage.bind(this)
        this.handleKeyDown = this.handleKeyDown.bind(this)
    }

    updateInputText(value) {
        this.setState({inputText:value})
    }

    handleKeyDown(event) {
        if (event.key === 'Enter') {
            this.sendMessage()
        }
    }

    sendMessage() {
        const {inputText} = this.state
        const {socket} = this.props
        const name = socket.io.opts.query.name
        socket.emit("message",{userName:name,message:inputText,id:socket.id})
        this.setState({inputText:''})
    }

    render() {
        const {socket} = this.props
        const name = socket.io.opts.query.name
        return (
            <div className="client">
                <h3>Hello {name}</h3>
                <h4>Welcome to Ultimate Q&A</h4>
                <ChatWindow userName={name}/>
                <div className="inputsContainer">
                    <input
                        className="msgInput"
                        type="text"
                        value={this.state.inputText}
                        onChange={(e) => this.updateInputText(e.target.value)}
                        onKeyDown={this.handleKeyDown}
                        autoFocus
                    />
                    <button className="sendBtn" onClick={this.sendMessage}>Send</button>
                </div>

            </div>
        );
    }
}

export default socketConnect(ChatClient);