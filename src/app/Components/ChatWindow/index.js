import React, {Component} from 'react';
import { socketConnect } from 'socket.io-react';
import classnames from 'classnames';
import './style.scss'

class ChatWindow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: []
        }
    }

    componentDidMount() {
        const {socket} = this.props

        socket.on('message', function (message) {
            this.setState({
                messages : this.state.messages.concat([message])
            })
        }.bind(this))

        socket.on('messages', function (messages) {
            this.setState({
                messages : messages
            })
        }.bind(this))
    }

    render() {
        return (
            <div className="chatWindow">
                {this.state.messages.map( (msg, index) => {
                    const msgClasses = classnames({
                        msg: true,
                        self: msg.userName === this.props.userName,
                        boti: msg.userName === 'boti',
                    })
                    return (
                        <div className={msgClasses} key={index}>
                            <div className="msgWrapper">
                                <div className="userName">{msg.userName}</div>
                                <div className="msgText">{msg.message}</div>
                            </div>
                        </div>
                    )
                })}
                <div id="anchor"/>
            </div>
        );
    }
}

export default socketConnect(ChatWindow);